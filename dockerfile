FROM python:3.7.4-alpine3.9

ENV APP_DIR=/app

WORKDIR $APP_DIR

# SOFTWARE PACKAGES
#   * musl: standard C library
#   * linux-headers: commonly needed, and an unusual package name from Alpine.
#   * build-base: used so we include the basic development packages (gcc)
#   * bash: so we can access /bin/bash
#   * git: to ease up clones of repos
#   * ca-certificates: for SSL verification during Pip and easy_install
#   * freetype: library used to render text onto bitmaps, and provides support font-related operations
#   * libgfortran: contains a Fortran shared library, needed to run Fortran
#   * libgcc: contains shared code that would be inefficient to duplicate every time as well as auxiliary helper routines and runtime support
#   * libstdc++: The GNU Standard C++ Library. This package contains an additional runtime library for C++ programs built with the GNU compiler
#   * openblas: open source implementation of the BLAS(Basic Linear Algebra Subprograms) API with many hand-crafted optimizations for specific processor types
#   * tcl: scripting language
#   * tk: GUI toolkit for the Tcl scripting language

RUN apk add --no-cache --update \
    musl \
    linux-headers \
    git \
    bash \
    ca-certificates \
    libgfortran \
    libgcc \
    libstdc++ \
    tcl \
    tk \
    freetype \
    libffi \
    libffi-dev \
    openssl-dev \
    openssh-client \
    py3-pip \
    python3 \
    python3-dev \
    build-base \
    tar \
    gfortran \
    perl 

RUN pip install --upgrade pip && pip install cryptography

ENV POETRY_VERSION=0.12.11
RUN pip install "poetry==$POETRY_VERSION"

RUN cp /etc/apk/repositories /etc/apk/repositories_orig \
    && sed -i -e 's/v[[:digit:]]\.[[:digit:]]/edge/g' /etc/apk/repositories \
    && apk add --no-cache --update tzdata \
    && mv /etc/apk/repositories_orig /etc/apk/repositories

COPY requirements.txt ./

RUN pip install --no-build-isolation --no-cache-dir -r requirements.txt

COPY src .

ENV ENV_FOR_DYNACONF=local
ENV FLASK_APP=app.py

ENTRYPOINT ["/bin/sh", "-c", \
            "gunicorn -b 0.0.0.0:80 -k gevent -w 4 --reload app:app"]