# FORUM API

## FORMAT CODE BEFORE COMMIT

Execute pre-commit: 

```bash
pre-commit install
```

Resolve code with `BLACK` before commit:

```bash
black --line-length 120 --target-version py37 src/
```

Valid code with `FLAKE8` before commit:

```bash
flake8 --max-line-length=120 --ignore=F401,W503,E203 --exclude src/config.py,test src/
```

## View log

tail -f -n 100 log.log | jq .

## Configure env vars

```bash
# local
dynaconf write toml -v FLASK_DEBUG=true -v FLASK_PORT=8000 -v FLASK_HOST=127.0.0.1 -s FLASK_SECRET_KEY=forum_api -v FORUM_DATABASE=forumdb -v FORUM_DATABASE_USER=root -s FORUM_DATABASE_PASS=root -v FORUM_DATABASE_HOST=127.0.0.1 -v FORUM_DATABASE_PORT=3306 -e local --path ./src
```