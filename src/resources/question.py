import pendulum
from common.dao.questions_query import QuestionDAO
from common.utils import helpers
from common.utils.enums import response
from common.utils.loggins import logger
from flask import request
from flask_restful import Resource, reqparse


class Question(Resource):
    def get(self, id=None):

        data = dict()
        data["page"] = request.args.get("page")
        data["page_size"] = request.args.get("page_size")
        data["title"] = request.args.get("title", None)

        if data["page"] and data["page_size"]:
            data["page"] = 1 if int(data["page"]) <= 0 else int(data["page"])
            data["page_size"] = 50 if int(data["page_size"]) <= 0 else int(data["page_size"])
            offset = data["page_size"] * (data["page"] - 1)
            data["offset"] = offset
            data["now"] = pendulum.now("America/Sao_Paulo")

            q = QuestionDAO()
            q.read_all(data)
            body = response("list_paged", data=q.result, total=q.total)
            logger.info("questions were returned successfully!")
            return body, 200

        body = response("error", msg="was not possible to list")
        return body, 500

    def post(self):
        post_parser = reqparse.RequestParser()
        post_parser.add_argument(
            "title", required=True, dest="title", type=str, location="json", help="Error field: {error_msg}",
        )
        post_parser.add_argument(
            "text", required=True, dest="text", type=str, location="json", help="Error field: {error_msg}",
        )

        post_parser.add_argument(
            "tags", dest="tags", type=dict, action="append", location="json", help="Error field: {error_msg}"
        )

        args = post_parser.parse_args()
        args["user_id"] = helpers.generate_user_id()
        args["title"] = args["title"].capitalize()

        try:
            q = QuestionDAO()
            q.create(args)
        except Exception as e:
            body = response("error", msg=str(e))
            return body, 500

        logger.info("created question successfully!")
        return q.result_obj, 201
