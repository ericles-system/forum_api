import unittest

from app import app


class FlaskTestAnswers(unittest.TestCase):

    # check if response is 200
    def test_answers_index(self):
        tester = app.test_client(self)
        response = tester.get("/api/questions/5/answers?page=1&page_size=10")
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

    # check if content return is application/json
    def test_answers_index_content(self):
        tester = app.test_client(self)
        response = tester.get("/api/questions/5/answers?page=1&page_size=10")
        self.assertEqual(response.content_type, "application/json")

    # check for data returned
    def test_answers_index_data(self):
        tester = app.test_client(self)
        response = tester.get("/api/questions/5/answers?page=1&page_size=10")
        self.assertTrue(b"data" in response.data)
        self.assertTrue(b"total" in response.data)

    # check if response is 200
    def test_answers_post_index(self):
        tester = app.test_client(self)
        body = {
         "text": "Resposta do test unitario"
        }
        response = tester.post("/api/questions/5/answers", json=body)
        statuscode = response.status_code
        self.assertEqual(statuscode, 201)

class FlaskTestLikes(unittest.TestCase):
    # check if response is 200
    def test_like_update_question(self):
        tester = app.test_client(self)
        body = {
            "type": "questions",
            "id": 18
        }
        response = tester.put("/api/likes", json=body)
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

    # check if content return is application/json
    def test_like_update_question_content(self):
        tester = app.test_client(self)
        body = {
            "type": "questions",
            "id": 18
        }
        response = tester.put("/api/likes", json=body)
        self.assertEqual(response.content_type, "application/json")

    # check for data returned
    def test_like_update_question_data(self):
        tester = app.test_client(self)
        body = {
            "type": "questions",
            "id": 18
        }
        response = tester.put("/api/likes", json=body)
        self.assertTrue(b"questions" in response.data)

   # check if response is 200
    def test_like_update_answers(self):
        tester = app.test_client(self)
        body = {
            "type": "answers",
            "id": 7
        }
        response = tester.put("/api/likes", json=body)
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

    # check if content return is application/json
    def test_like_update_answers_content(self):
        tester = app.test_client(self)
        body = {
            "type": "answers",
            "id": 7
        }
        response = tester.put("/api/likes", json=body)
        self.assertEqual(response.content_type, "application/json")

    # check for data returned
    def test_like_update_answers_data(self):
        tester = app.test_client(self)
        body = {
            "type": "answers",
            "id": 7
        }
        response = tester.put("/api/likes", json=body)
        self.assertTrue(b"answers" in response.data)

class FlaskTestTags(unittest.TestCase):
    # check if response is 200
    def test_tags_index(self):
        tester = app.test_client(self)
        response = tester.get("/api/tags")
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

    # check if content return is application/json
    def test_tags_index_content(self):
        tester = app.test_client(self)
        response = tester.get("/api/tags")
        self.assertEqual(response.content_type, "application/json")

    # check for data returned
    def test_tags_index_data(self):
        tester = app.test_client(self)
        response = tester.get("/api/tags")
        self.assertTrue(b"data" in response.data)

class FlaskTestQuestions(unittest.TestCase):
    # check if response is 200
    def test_question_index(self):
        tester = app.test_client(self)
        response = tester.get("/api/questions?page=1&page_size=10")
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

    # check if content return is application/json
    def test_question_index_content(self):
        tester = app.test_client(self)
        response = tester.get("/api/questions?page=1&page_size=10")
        self.assertEqual(response.content_type, "application/json")

    # check for data returned
    def test_question_index_data(self):
        tester = app.test_client(self)
        response = tester.get("/api/questions?page=1&page_size=10")
        self.assertTrue(b"data" in response.data)
        self.assertTrue(b"total" in response.data)

    def test_question_byname(self):
        tester = app.test_client(self)
        response = tester.get("/api/questions?page=1&page_size=10&title=como")
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

    # check if content return is application/json
    def test_question_byname_content(self):
        tester = app.test_client(self)
        response = tester.get("/api/questions?page=1&page_size=10&title=como")
        self.assertEqual(response.content_type, "application/json")

    # check for data returned
    def test_question_byname_data(self):
        tester = app.test_client(self)
        response = tester.get("/api/questions?page=1&page_size=10&title=como")
        self.assertTrue(b"data" in response.data)
        self.assertTrue(b"total" in response.data)

    # check if response is 200
    def test_question_post_index(self):
        tester = app.test_client(self)
        body = {
                "title": "Testando",
                "text": "Test unitario 2",
                "tags": [
                    {
                        "tag_id": 1

                    },
                    {
                        "tag_id": 2
                    }
                    
                ]
        }
        response = tester.post("/api/questions", json=body)
        statuscode = response.status_code
        self.assertEqual(statuscode, 201)


if __name__ == "__main__":
    unittest.main()
