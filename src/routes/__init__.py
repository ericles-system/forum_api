from flask import Blueprint
from flask_restful import Api

# resources
from resources._ping import PingStatus
from resources.question import Question
from resources.answer import Answers
from resources.tags import Tags
from resources.likes import Likes

api_bp = Blueprint("api", __name__)
api = Api(api_bp)

# Ping Status
api.add_resource(PingStatus, "/_ping")

# Questions
api.add_resource(Question, "/questions", "/questions/<int:id>")

# Answers
api.add_resource(Answers, "/questions/<int:question_id>/answers")

# Tags
api.add_resource(Tags, "/tags", "/tags/<int:id>")

# Likes
api.add_resource(Likes, "/likes")
