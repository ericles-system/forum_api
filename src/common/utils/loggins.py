from config import LOGGER_FILE, LOGGER_LEVEL
from loguru import logger

formatter = "[ {level.icon}{level:^10}] {time:YYYY-MM-DD hh:mm:ss} {file} - {name}: {message}"

try:
    level = LOGGER_LEVEL
except AttributeError:
    level = "INFO"
try:
    file_name = LOGGER_FILE
except AttributeError:
    file_name = "log.log"

logger.add(file_name, format=formatter, level=level, rotation="500 MB", colorize=True, serialize=True)


if __name__ == "__main__":
    logger.debug("debug message")
    logger.info("info message")
    logger.warning("warn message")
    logger.error("error message")
    logger.critical("critical message")
