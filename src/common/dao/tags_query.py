import dataset
from config import LOGGER_LEVEL, MYSQL_URL

from .model import ModelBase


class TagsDAO(ModelBase):
    def __init__(self):
        self.result = []
        self.loglevel = True if LOGGER_LEVEL.upper() == "DEBUG" else False

    def read_all(self, parameters=None):
        with dataset.connect(url=MYSQL_URL, engine_kwargs={"echo": self.loglevel}) as db:
            data = db["tags"].all()
            for d in data:
                self.result.append(d)

    def create(self, parameters):
        pass
