import abc


class ModelBase(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def read_all(self, parameters):
        return

    @abc.abstractmethod
    def create(self, parameters):
        return
