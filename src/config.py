from dynaconf import LazySettings
from dynaconf import Validator
from sqlalchemy.engine import url

settings = LazySettings()

settings.validators.register(
    Validator(
        "FORUM_DATABASE_PORT",
        "FORUM_DATABASE_USER",
        "FORUM_DATABASE_PASS",
        "FORUM_DATABASE",
        "FORUM_DATABASE_HOST",
        must_exist=True,
    ),
    Validator("FLASK_PORT", must_exist=True, eq=8000, when=Validator("FORUM_DATABASE_HOST", eq="127.0.0.1"),),
    Validator("FLASK_DEBUG", "FLASK_HOST", "FLASK_SECRET_KEY", must_exist=True),
)

# in case of error, interrupt the application
settings.validators.validate()

FLASK_DEBUG = (settings.get("FLASK_DEBUG"),)
FLASK_PORT = settings.get("FLASK_PORT")
FLASK_HOST = settings.get("FLASK_HOST")
FLASK_SECRET_KEY = settings.get("FLASK_SECRET_KEY")
LOGGER_LEVEL = settings.get("LOGGER_LEVEL", "INFO")
LOGGER_FILE = settings.get("LOGGER_FILE", "log.log")

MYSQL_E2M_DATABASE_CONN = {
    "drivername": "mysql+pymysql",
    "host": settings.get("FORUM_DATABASE_HOST", fresh=True),
    "port": settings.get("FORUM_DATABASE_PORT", 3306, fresh=True),
    "username": settings.get("FORUM_DATABASE_USER", fresh=True),
    "password": settings.get("FORUM_DATABASE_PASS", fresh=True),
    "database": settings.get("FORUM_DATABASE", fresh=True),
}

MYSQL_URL = str(url.URL(**MYSQL_E2M_DATABASE_CONN)) + "?charset=utf8"
